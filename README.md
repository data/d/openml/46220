# OpenML dataset: Weather-Saaleaue

https://www.openml.org/d/46220

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Weather measures from Saaleaue provided by the Max-Planck-Institute for Biogeochemistry.

Several weather measures provided by Max-Planck-Institute for Biogeochemistry from the Weather Station on Top of the Roof of the Institute Building.

We have assembled all the files available as of 24-05-2024 on https://www.bgc-jena.mpg.de/wetter/weather_data.html

There are 32 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d %H:%M:%S".

time_step: The time step on the time series.

value_X (X from 0 to 20): The values of the time series, which will be used for the forecasting task.

Preprocessing:

1 - Renamed column 'Date Time' to 'date'

2 - Parsed the date with the format '%d.%m.%Y %H:%M:%S' and converted it to string with format %Y-%m-%d %H:%M:%S.

3 - Replaced values of -9999 to nan.

Values of -9999 seems to indicate a problem with the measure. Besides, it seems that some measures only started to be collected later on the year of 2002.

4 - Renamed columns [1:] to 'value_X' with X from 0 to 20.

5 - Rounded 'date' to the nearest 10 minutes.

Some 'date' values were not exactly at 10 minutes frequency (offset by some seconds or by 1 minute for '2011-09-26 13:41:00', '2012-07-24 06:51:00', '2013-08-23 10:11:00').

6 - Created 'id_series' with value 0. There is only one multivariate time series.

7 - Ensured that there are no missing dates and that the frequency of the time_series is 10 minutes. Filled the missing dates with NaNs.

8 - Created 'time_step' column from 'date' and 'id_series' with increasing values from 0 to the size of the time series.

9 - Casted 'date' to str, 'time_step' to int, 'value_X' to float, and defined 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46220) of an [OpenML dataset](https://www.openml.org/d/46220). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46220/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46220/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46220/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

